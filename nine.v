// DSCH3
// 12/1/2015 7:43:43 PM
// C:\Users\AcidRobot\github\clock_work\nine.sch

module nine( clk1,in1,in2,in3,digit22,digit21,digit20,out1,
 digit1,digit2,digit3,digit4);
 input clk1,in1,in2,in3;
 output digit22,digit21,digit20,out1,digit1,digit2,digit3,digit4;
 wire w2,w5,w6,w7,w8,w11,w12,w13;
 wire w14,w16,w18,w19,w22,w23,w24,w27;
 wire w28,w29,w30,w31,w34,w35,w36,w37;
 wire w38,w39,w41,w42,w43,w44,w45,w46;
 wire w47;
 not #(10) inv_1(out1,w2);
 xor #(16) xor2_2(w6,digit21,w5);
 and #(16) and2_3(w8,w7,w6);
 dreg #(12) dreg_4(digit21,w11,w8,in2,clk1);
 and #(23) and2_5(w12,digit21,w5);
 and #(16) and2_6(w14,w7,w13);
 dreg #(12) dreg_7(digit20,w16,w14,in2,clk1);
 and #(37) and2_8(w19,in3,w18);
 xor #(16) xor2_9(w22,digit4,in1);
 dreg #(12) dreg_10(digit4,w24,w23,in2,clk1);
 and #(16) and2_11(w23,w19,w22);
 and #(23) and2_12(w5,digit20,w27);
 xor #(16) xor2_13(w28,digit22,w12);
 and #(16) and2_14(w29,w7,w28);
 xor #(16) xor2_15(w13,digit20,w27);
 dreg #(12) dreg_16(digit22,w30,w29,in2,clk1);
 and #(9) and2_17(w31,digit22,w12);
 nand #(20) nand3_18(w2,digit22,digit20,w27);
 and #(30) and2_19(w7,in3,w2);
 nand #(20) nand3_20(w18,digit1,digit4,in1);
 and #(23) and2_21(w35,digit2,w34);
 dreg #(12) dreg_22(digit2,w37,w36,in2,clk1);
 and #(16) and2_23(w36,w19,w38);
 xor #(16) xor2_24(w38,digit2,w34);
 and #(23) and2_25(w39,digit4,in1);
 xor #(16) xor2_26(w41,digit1,w35);
 and #(16) and2_27(w42,w19,w41);
 dreg #(12) dreg_28(digit1,w43,w42,in2,clk1);
 and #(9) and2_29(w44,digit1,w35);
 and #(23) and2_30(w34,digit3,w39);
 dreg #(12) dreg_31(digit3,w46,w45,in2,clk1);
 and #(16) and2_32(w45,w19,w47);
 xor #(16) xor2_33(w47,digit3,w39);
 not #(24) inv_34(w27,w18);
endmodule

// Simulation parameters in Verilog Format
always
#1000 clk1=~clk1;
#1000 in1=~in1;
#2000 in2=~in2;
#4000 in3=~in3;

// Simulation parameters
// clk1 CLK 10.00 10.00
// in1 CLK 10 10
// in2 CLK 20 20
// in3 CLK 40 40
