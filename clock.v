// DSCH3
// 12/1/2015 10:05:48 PM
// C:\Users\Acid_Robot\git_repo\clock_work\clock.sch

module clock( clk2,in4,in5,in6,digit3,digit4,digit1,digit2,
 digit3,digit4,digit2,digit3,digit4,digit1,digit2,digit3,
 digit4,digit1,digit2,digit3,digit4,digit2,digit3,digit4);
 input clk2,in4,in5,in6;
 output digit3,digit4,digit1,digit2,digit3,digit4,digit2,digit3;
 output digit4,digit1,digit2,digit3,digit4,digit1,digit2,digit3;
 output digit4,digit2,digit3,digit4;
 wire w2,w3,w4,w5,w6,w7,w8,w9;
 wire w10,w11,w12,w13,w14,w15,w16,w17;
 wire w18,w19,w20,w21,w22,w23,w24,w25;
 wire w26,w27,w28,w29,w30,w31,w32,w33;
 wire w34,w35,w36,w37,w38,w39,w40,w41;
 wire w42,w43,w44,w45,w46,w47,w48,w49;
 wire w50,w51,w52,w53,w54,w55,w56,w57;
 wire w58,w59,w60,w61,w62,w63,w64,w65;
 wire w66,w67,w68,w69,w70,w71,w72,w73;
 wire w74,w75,w76,w77,w78,w79,w80,w81;
 wire w82,w83,w84,w85,w86,w87,w88,w89;
 wire w90,w91,w92,w93,w94,w95,w96,w97;
 wire w98,w99,w100,w101,w102,w103,w104,w105;
 wire w106,w107,w108,w109,w110,w111,w112,w113;
 wire w114,w115,w116,w117,w118,w119,w120,w121;
 wire w122,w123,w124,w125,w126;
endmodule

// Simulation parameters in Verilog Format
always
#2000 clk2=~clk2;
#1000 in4=~in4;
#2000 in5=~in5;
#4000 in6=~in6;

// Simulation parameters
// clk2 CLK 20.00 20.00
// in4 CLK 10 10
// in5 CLK 20 20
// in6 CLK 40 40
