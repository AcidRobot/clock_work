// DSCH3
// 11/26/2016 4:19:21 PM
// C:\Users\AcidRobot\github\clock_work\twelve.sch

module twelve( clk1,in1,in2,in3,digit23,digit22,digit21,digit20,
 digit13,digit12,digit11,digit10);
 input clk1,in1,in2,in3;
 output digit23,digit22,digit21,digit20,digit13,digit12,digit11,digit10;
 wire w2,w3,w4,w6,w7,w10,w11,w12;
 wire w14,w15,w16,w17,w18,w20,w22,w25;
 wire w26,w27,w28,w29,w30,w31,w32,w33;
 wire w34,w36,w37,w38,w39,w40,w41;
 xor #(16) xor2_1(w4,w2,w3);
 not #(10) inv_2(w6,digit13);
 dreg #(12) dreg_3(digit13,w10,w7,in2,clk1);
 and #(16) and2_4(w12,w3,w11);
 and #(23) and2_5(w15,digit11,w14);
 and #(16) and2_6(w18,w16,w17);
 dreg #(12) dreg_7(digit20,w20,w18,in2,clk1);
 and #(37) and2_8(w3,in3,w22);
 xor #(16) xor2_9(w25,digit10,in1);
 dreg #(12) dreg_10(digit10,w27,w26,in2,clk1);
 and #(16) and2_11(w26,w4,w25);
 not #(17) inv_12(w28,w22);
 and #(9) and2_13(w30,digit13,w29);
 dreg #(12) dreg_14(digit11,w31,w12,in2,clk1);
 xor #(16) xor2_15(w17,digit20,w28);
 xor #(23) xor2_16(w22,w2,w32);
 xor #(16) xor2_17(w11,digit11,w14);
 nand #(13) nand3_18(w34,w33,digit20,w28);
 and #(16) and2_19(w16,in3,w34);
 nand #(13) nand3_20(w32,digit13,digit10,in1);
 and #(23) and2_21(w29,digit12,w15);
 dreg #(12) dreg_22(digit12,w37,w36,in2,clk1);
 and #(16) and2_23(w36,w3,w38);
 xor #(16) xor2_24(w38,digit12,w15);
 and #(23) and2_25(w14,digit10,in1);
 xor #(16) xor2_26(w39,digit13,w29);
 and #(16) and2_27(w7,w3,w39);
 and #(23) and4_28(w33,w40,digit11,w6,w41);
 not #(10) inv_29(w41,digit12);
 not #(10) inv_30(w40,digit10);
 and #(23) and2_31(w2,digit20,w33);
endmodule

// Simulation parameters in Verilog Format
always
#1000 clk1=~clk1;
#1000 in1=~in1;
#2000 in2=~in2;
#4000 in3=~in3;

// Simulation parameters
// clk1 CLK 10.000 10.000
// in1 CLK 10 10
// in2 CLK 20 20
// in3 CLK 40 40
