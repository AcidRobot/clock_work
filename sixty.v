// DSCH3
// 11/26/2016 4:38:41 PM
// C:\Users\AcidRobot\github\clock_work\sixty.sch

module sixty( clk1,in1,in2,in3,digit22,digit21,digit20,out2,
 digit13,digit12,digit11,digit10);
 input clk1,in1,in2,in3;
 output digit22,digit21,digit20,out2,digit13,digit12,digit11,digit10;
 wire w2,w5,w6,w7,w8,w11,w12,w13;
 wire w14,w16,w18,w19,w22,w23,w24,w27;
 wire w28,w29,w30,w31,w34,w35,w36,w37;
 wire w38,w39,w40,w41,w42,w44,w45,w46;
 not #(10) inv_1(out2,w2);
 xor #(16) xor2_2(w6,digit21,w5);
 and #(16) and2_3(w8,w7,w6);
 dreg #(12) dreg_4(digit21,w11,w8,in2,clk1);
 and #(23) and2_5(w12,digit21,w5);
 and #(16) and2_6(w14,w7,w13);
 dreg #(12) dreg_7(digit20,w16,w14,in2,clk1);
 and #(37) and2_8(w19,in3,w18);
 xor #(16) xor2_9(w22,digit10,in1);
 dreg #(12) dreg_10(digit10,w24,w23,in2,clk1);
 and #(16) and2_11(w23,w19,w22);
 and #(23) and2_12(w5,digit20,w27);
 xor #(16) xor2_13(w28,digit22,w12);
 and #(16) and2_14(w29,w7,w28);
 xor #(16) xor2_15(w13,digit20,w27);
 dreg #(12) dreg_16(digit22,w30,w29,in2,clk1);
 and #(9) and2_17(w31,digit22,w12);
 nand #(20) nand3_18(w2,digit22,digit20,w27);
 and #(30) and2_19(w7,in3,w2);
 nand #(20) nand3_20(w18,digit13,digit10,in1);
 and #(16) and2_21(w35,digit12,w34);
 dreg #(12) dreg_22(digit12,w37,w36,in2,clk1);
 and #(16) and2_23(w36,w19,w38);
 xor #(16) xor2_24(w38,digit12,w34);
 and #(23) and2_25(w39,digit10,in1);
 not #(24) inv_26(w27,w18);
 xor #(16) xor2_27(w40,digit13,w35);
 and #(16) and2_28(w41,w19,w40);
 dreg #(12) dreg_29(digit13,w42,w41,in2,clk1);
 and #(23) and2_30(w34,digit11,w39);
 dreg #(12) dreg_31(digit11,w45,w44,in2,clk1);
 and #(16) and2_32(w44,w19,w46);
 xor #(16) xor2_33(w46,digit11,w39);
endmodule

// Simulation parameters in Verilog Format
always
#500 clk1=~clk1;
#1000 in1=~in1;
#2000 in2=~in2;
#4000 in3=~in3;

// Simulation parameters
// clk1 CLK 5.000 5.000
// in1 CLK 10 10
// in2 CLK 20 20
// in3 CLK 40 40
