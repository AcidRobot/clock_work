// DSCH3
// 12/1/2015 10:40:06 PM
// C:\Users\AcidRobot\github\clock_work\one.sch

module one( clk1,in1,in2,in3,digit21,digit20,out1,digit1,
 digit2,digit3,digit4);
 input clk1,in1,in2,in3;
 output digit21,digit20,out1,digit1,digit2,digit3,digit4;
 wire w2,w4,w5,w7,w8,w9,w10,w13;
 wire w15,w16,w20,w21,w22,w27,w28,w29;
 wire w30,w31,w32,w33,w34,w35,w37,w38;
 wire w39,w40,w41,w42;
 not #(10) inv_1(out1,w2);
 not #(24) inv_2(w5,w4);
 xor #(16) xor2_3(w8,digit3,w7);
 and #(16) and2_4(w10,w9,w8);
 dreg #(12) dreg_5(digit3,w13,w10,in2,clk1);
 and #(58) and2_6(in2,digit21,w15);
 xor #(16) xor2_7(w16,digit21,w15);
 and #(37) and2_8(w9,in3,w4);
 xor #(16) xor2_9(w20,digit4,in1);
 dreg #(12) dreg_10(digit4,w22,w21,in2,clk1);
 and #(16) and2_11(w21,w9,w20);
 xor #(16) xor2_12(w28,digit1,w27);
 and #(23) and2_13(w29,digit3,w7);
 and #(9) and2_14(w30,digit1,w27);
 dreg #(12) dreg_15(digit21,w32,w31,in2,clk1);
 dreg #(12) dreg_16(digit1,w34,w33,in2,clk1);
 and #(16) and2_17(w33,w9,w28);
 nand #(20) nand3_18(w2,digit21,digit20,w5);
 and #(23) and2_19(w35,in3,w2);
 nand #(20) nand3_20(w4,digit1,digit4,in1);
 and #(23) and2_21(w27,digit2,w29);
 dreg #(12) dreg_22(digit2,w38,w37,in2,clk1);
 and #(16) and2_23(w37,w9,w39);
 xor #(16) xor2_24(w39,digit2,w29);
 and #(23) and2_25(w7,digit4,in1);
 and #(16) and2_26(w31,w35,w16);
 and #(16) and2_27(w41,w35,w40);
 dreg #(12) dreg_28(digit20,w42,w41,in2,clk1);
 xor #(16) xor2_29(w40,digit20,w5);
 and #(23) and2_30(w15,digit20,w5);
endmodule

// Simulation parameters in Verilog Format
always
#1000 clk1=~clk1;
#1000 in1=~in1;
#2000 in2=~in2;
#4000 in3=~in3;

// Simulation parameters
// clk1 CLK 10.00 10.00
// in1 CLK 10 10
// in2 CLK 20 20
// in3 CLK 40 40
