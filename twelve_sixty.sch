DSCH3
VERSION 11/26/2016 5:14:10 PM
BB(-430,-40,-105,138)
SYM  #sixty
BB(-175,-5,-140,85)
TITLE -150 -12  #sixty
MODEL 6000
PROP                                                                                                                                    
REC(-170,0,25,80,r)
VIS 5
PIN(-140,35,0.000,0.000)clk1
PIN(-140,25,0.000,0.000)in1
PIN(-140,15,0.000,0.000)in2
PIN(-140,5,0.000,0.000)in3
PIN(-175,15,0.060,0.350)digit22
PIN(-175,25,0.060,0.280)digit21
PIN(-175,35,0.060,0.350)digit20
PIN(-175,5,0.060,0.280)out2
PIN(-175,45,0.060,0.280)digit13
PIN(-175,55,0.060,0.280)digit12
PIN(-175,65,0.060,0.280)digit11
PIN(-175,75,0.060,0.350)digit10
LIG(-140,35,-145,35)
LIG(-140,25,-145,25)
LIG(-140,15,-145,15)
LIG(-140,5,-145,5)
LIG(-170,15,-175,15)
LIG(-170,25,-175,25)
LIG(-170,35,-175,35)
LIG(-170,5,-175,5)
LIG(-170,45,-175,45)
LIG(-170,55,-175,55)
LIG(-170,65,-175,65)
LIG(-170,75,-175,75)
LIG(-145,0,-145,80)
LIG(-145,0,-170,0)
LIG(-170,0,-170,80)
LIG(-170,80,-145,80)
VLG   module sixty( clk1,in1,in2,in3,digit22,digit21,digit20,out2,
VLG    digit13,digit12,digit11,digit10);
VLG    input clk1,in1,in2,in3;
VLG    output digit22,digit21,digit20,out2,digit13,digit12,digit11,digit10;
VLG    wire w2,w5,w6,w7,w8,w11,w12,w13;
VLG    wire w14,w16,w18,w19,w22,w23,w24,w27;
VLG    wire w28,w29,w30,w31,w34,w35,w36,w37;
VLG    wire w38,w39,w40,w41,w42,w44,w45,w46;
VLG    not #(10) inv_1(out2,w2);
VLG    xor #(16) xor2_2(w6,digit21,w5);
VLG    and #(16) and2_3(w8,w7,w6);
VLG    dreg #(12) dreg_4(digit21,w11,w8,in2,clk1);
VLG    and #(23) and2_5(w12,digit21,w5);
VLG    and #(16) and2_6(w14,w7,w13);
VLG    dreg #(12) dreg_7(digit20,w16,w14,in2,clk1);
VLG    and #(37) and2_8(w19,in3,w18);
VLG    xor #(16) xor2_9(w22,digit10,in1);
VLG    dreg #(12) dreg_10(digit10,w24,w23,in2,clk1);
VLG    and #(16) and2_11(w23,w19,w22);
VLG    and #(23) and2_12(w5,digit20,w27);
VLG    xor #(16) xor2_13(w28,digit22,w12);
VLG    and #(16) and2_14(w29,w7,w28);
VLG    xor #(16) xor2_15(w13,digit20,w27);
VLG    dreg #(12) dreg_16(digit22,w30,w29,in2,clk1);
VLG    and #(9) and2_17(w31,digit22,w12);
VLG    nand #(20) nand3_18(w2,digit22,digit20,w27);
VLG    and #(30) and2_19(w7,in3,w2);
VLG    nand #(20) nand3_20(w18,digit13,digit10,in1);
VLG    and #(16) and2_21(w35,digit12,w34);
VLG    dreg #(12) dreg_22(digit12,w37,w36,in2,clk1);
VLG    and #(16) and2_23(w36,w19,w38);
VLG    xor #(16) xor2_24(w38,digit12,w34);
VLG    and #(23) and2_25(w39,digit10,in1);
VLG    not #(24) inv_26(w27,w18);
VLG    xor #(16) xor2_27(w40,digit13,w35);
VLG    and #(16) and2_28(w41,w19,w40);
VLG    dreg #(12) dreg_29(digit13,w42,w41,in2,clk1);
VLG    and #(23) and2_30(w34,digit11,w39);
VLG    dreg #(12) dreg_31(digit11,w45,w44,in2,clk1);
VLG    and #(16) and2_32(w44,w19,w46);
VLG    xor #(16) xor2_33(w46,digit11,w39);
VLG   endmodule
FSYM
SYM  #twelve
BB(-370,10,-335,100)
TITLE -345 3  #twelve
MODEL 6000
PROP                                                                                                                                   
REC(-365,15,25,80,r)
VIS 5
PIN(-335,50,0.000,0.000)clk1
PIN(-335,40,0.000,0.000)in1
PIN(-335,30,0.000,0.000)in2
PIN(-335,20,0.000,0.000)in3
PIN(-370,20,0.060,0.070)digit23
PIN(-370,30,0.060,0.070)digit22
PIN(-370,40,0.060,0.070)digit21
PIN(-370,50,0.060,0.350)digit20
PIN(-370,60,0.060,0.420)digit13
PIN(-370,70,0.060,0.350)digit12
PIN(-370,80,0.060,0.350)digit11
PIN(-370,90,0.060,0.420)digit10
LIG(-335,50,-340,50)
LIG(-335,40,-340,40)
LIG(-335,30,-340,30)
LIG(-335,20,-340,20)
LIG(-365,20,-370,20)
LIG(-365,30,-370,30)
LIG(-365,40,-370,40)
LIG(-365,50,-370,50)
LIG(-365,60,-370,60)
LIG(-365,70,-370,70)
LIG(-365,80,-370,80)
LIG(-365,90,-370,90)
LIG(-340,15,-340,95)
LIG(-340,15,-365,15)
LIG(-365,15,-365,95)
LIG(-365,95,-340,95)
VLG  module twelve( clk1,in1,in2,in3,digit23,digit22,digit21,digit20,
VLG   digit13,digit12,digit11,digit10);
VLG   input clk1,in1,in2,in3;
VLG   output digit23,digit22,digit21,digit20,digit13,digit12,digit11,digit10;
VLG   wire w2,w3,w4,w6,w7,w10,w11,w12;
VLG   wire w14,w15,w16,w17,w18,w20,w22,w25;
VLG   wire w26,w27,w28,w29,w30,w31,w32,w33;
VLG   wire w34,w36,w37,w38,w39,w40,w41;
VLG   xor #(16) xor2_1(w4,w2,w3);
VLG   not #(10) inv_2(w6,digit13);
VLG   dreg #(12) dreg_3(digit13,w10,w7,in2,clk1);
VLG   and #(16) and2_4(w12,w3,w11);
VLG   and #(23) and2_5(w15,digit11,w14);
VLG   and #(16) and2_6(w18,w16,w17);
VLG   dreg #(12) dreg_7(digit20,w20,w18,in2,clk1);
VLG   and #(37) and2_8(w3,in3,w22);
VLG   xor #(16) xor2_9(w25,digit10,in1);
VLG   dreg #(12) dreg_10(digit10,w27,w26,in2,clk1);
VLG   and #(16) and2_11(w26,w4,w25);
VLG   not #(17) inv_12(w28,w22);
VLG   and #(9) and2_13(w30,digit13,w29);
VLG   dreg #(12) dreg_14(digit11,w31,w12,in2,clk1);
VLG   xor #(16) xor2_15(w17,digit20,w28);
VLG   xor #(23) xor2_16(w22,w2,w32);
VLG   xor #(16) xor2_17(w11,digit11,w14);
VLG   nand #(13) nand3_18(w34,w33,digit20,w28);
VLG   and #(16) and2_19(w16,in3,w34);
VLG   nand #(13) nand3_20(w32,digit13,digit10,in1);
VLG   and #(23) and2_21(w29,digit12,w15);
VLG   dreg #(12) dreg_22(digit12,w37,w36,in2,clk1);
VLG   and #(16) and2_23(w36,w3,w38);
VLG   xor #(16) xor2_24(w38,digit12,w15);
VLG   and #(23) and2_25(w14,digit10,in1);
VLG   xor #(16) xor2_26(w39,digit13,w29);
VLG   and #(16) and2_27(w7,w3,w39);
VLG   and #(23) and4_28(w33,w40,digit11,w6,w41);
VLG   not #(10) inv_29(w41,digit12);
VLG   not #(10) inv_30(w40,digit10);
VLG   and #(23) and2_31(w2,digit20,w33);
VLG  endmodule
FSYM
SYM  #digit
BB(-430,-40,-405,-5)
TITLE -430 -8  #digit4
MODEL 89
PROP                                                                                                                                   
REC(-425,-35,15,21,r)
VIS 4
PIN(-425,-5,0.000,0.000)digit43
PIN(-420,-5,0.000,0.000)digit42
PIN(-415,-5,0.000,0.000)digit41
PIN(-410,-5,0.000,0.000)digit40
LIG(-430,-40,-430,-10)
LIG(-405,-40,-430,-40)
LIG(-405,-10,-405,-40)
LIG(-430,-10,-405,-10)
LIG(-425,-10,-425,-5)
LIG(-420,-10,-420,-5)
LIG(-415,-10,-415,-5)
LIG(-410,-10,-410,-5)
FSYM
SYM  #digit
BB(-405,-40,-380,-5)
TITLE -405 -8  #digit3
MODEL 89
PROP                                                                                                                                   
REC(-400,-35,15,21,r)
VIS 4
PIN(-400,-5,0.000,0.000)digit33
PIN(-395,-5,0.000,0.000)digit32
PIN(-390,-5,0.000,0.000)digit31
PIN(-385,-5,0.000,0.000)digit30
LIG(-405,-40,-405,-10)
LIG(-380,-40,-405,-40)
LIG(-380,-10,-380,-40)
LIG(-405,-10,-380,-10)
LIG(-400,-10,-400,-5)
LIG(-395,-10,-395,-5)
LIG(-390,-10,-390,-5)
LIG(-385,-10,-385,-5)
FSYM
SYM  #clock
BB(-120,132,-105,138)
TITLE -110 135  #clock1
MODEL 69
PROP   2.528 2.528                                                                                                                               
REC(-113,133,6,4,r)
VIS 1
PIN(-120,135,1.500,0.840)clk1
LIG(-115,135,-120,135)
LIG(-110,133,-108,133)
LIG(-114,133,-112,133)
LIG(-115,132,-115,138)
LIG(-105,138,-105,132)
LIG(-110,137,-110,133)
LIG(-112,133,-112,137)
LIG(-112,137,-110,137)
LIG(-108,137,-106,137)
LIG(-108,133,-108,137)
LIG(-115,138,-105,138)
LIG(-115,132,-105,132)
FSYM
SYM  #button
BB(-115,121,-106,129)
TITLE -110 125  #button3
MODEL 59
PROP                                                                                                                                   
REC(-113,122,6,6,r)
VIS 1
PIN(-115,125,0.000,0.000)in3
LIG(-114,125,-115,125)
LIG(-106,129,-106,121)
LIG(-114,129,-106,129)
LIG(-114,121,-114,129)
LIG(-106,121,-114,121)
LIG(-107,128,-107,122)
LIG(-113,128,-107,128)
LIG(-113,122,-113,128)
LIG(-107,122,-113,122)
FSYM
SYM  #button
BB(-115,111,-106,119)
TITLE -110 115  #button2
MODEL 59
PROP                                                                                                                                   
REC(-113,112,6,6,r)
VIS 1
PIN(-115,115,0.000,0.000)in2
LIG(-114,115,-115,115)
LIG(-106,119,-106,111)
LIG(-114,119,-106,119)
LIG(-114,111,-114,119)
LIG(-106,111,-114,111)
LIG(-107,118,-107,112)
LIG(-113,118,-107,118)
LIG(-113,112,-113,118)
LIG(-107,112,-113,112)
FSYM
SYM  #button
BB(-115,101,-106,109)
TITLE -110 105  #button1
MODEL 59
PROP                                                                                                                                   
REC(-113,102,6,6,r)
VIS 1
PIN(-115,105,0.000,0.000)in1
LIG(-114,105,-115,105)
LIG(-106,109,-106,101)
LIG(-114,109,-106,109)
LIG(-114,101,-114,109)
LIG(-106,101,-114,101)
LIG(-107,108,-107,102)
LIG(-113,108,-107,108)
LIG(-113,102,-113,108)
LIG(-107,102,-113,102)
FSYM
SYM  #digit
BB(-225,-40,-200,-5)
TITLE -225 -8  #digit2
MODEL 89
PROP                                                                                                                                   
REC(-220,-35,15,21,r)
VIS 4
PIN(-220,-5,0.000,0.000)digit1
PIN(-215,-5,0.000,0.000)digit2
PIN(-210,-5,0.000,0.000)digit3
PIN(-205,-5,0.000,0.000)digit4
LIG(-225,-40,-225,-10)
LIG(-200,-40,-225,-40)
LIG(-200,-10,-200,-40)
LIG(-225,-10,-200,-10)
LIG(-220,-10,-220,-5)
LIG(-215,-10,-215,-5)
LIG(-210,-10,-210,-5)
LIG(-205,-10,-205,-5)
FSYM
SYM  #digit
BB(-200,-40,-175,-5)
TITLE -200 -8  #digit1
MODEL 89
PROP                                                                                                                                   
REC(-195,-35,15,21,r)
VIS 4
PIN(-195,-5,0.000,0.000)digit1
PIN(-190,-5,0.000,0.000)digit2
PIN(-185,-5,0.000,0.000)digit3
PIN(-180,-5,0.000,0.000)digit4
LIG(-200,-40,-200,-10)
LIG(-175,-40,-200,-40)
LIG(-175,-10,-175,-40)
LIG(-200,-10,-175,-10)
LIG(-195,-10,-195,-5)
LIG(-190,-10,-190,-5)
LIG(-185,-10,-185,-5)
LIG(-180,-10,-180,-5)
FSYM
CNC(-120 125)
CNC(-125 115)
CNC(-215 125)
CNC(-220 115)
CNC(-230 135)
CNC(-140 135)
CNC(-335 135)
CNC(-325 115)
CNC(-270 30)
CNC(-265 30)
CNC(-135 105)
CNC(-265 30)
CNC(-270 30)
CNC(-270 30)
CNC(-265 30)
CNC(-265 30)
CNC(-265 30)
CNC(-265 30)
CNC(-265 30)
LIG(-175,5,-225,5)
LIG(-230,10,-215,10)
LIG(-225,5,-225,30)
LIG(-270,30,-265,30)
LIG(-230,40,-230,135)
LIG(-215,10,-215,125)
LIG(-230,20,-220,20)
LIG(-220,20,-220,115)
LIG(-175,75,-180,75)
LIG(-180,-5,-180,75)
LIG(-185,-5,-185,65)
LIG(-185,65,-175,65)
LIG(-190,-5,-190,55)
LIG(-190,55,-175,55)
LIG(-195,-5,-195,45)
LIG(-195,45,-175,45)
LIG(-205,-5,-205,35)
LIG(-175,35,-205,35)
LIG(-140,35,-140,135)
LIG(-175,25,-210,25)
LIG(-210,-5,-210,25)
LIG(-175,15,-200,15)
LIG(-200,15,-200,0)
LIG(-200,0,-215,0)
LIG(-215,0,-215,-5)
LIG(-140,25,-135,25)
LIG(-140,5,-120,5)
LIG(-140,15,-125,15)
LIG(-120,5,-120,125)
LIG(-125,15,-125,115)
LIG(-135,25,-135,105)
LIG(-115,125,-120,125)
LIG(-140,135,-230,135)
LIG(-120,125,-215,125)
LIG(-115,115,-125,115)
LIG(-425,135,-335,135)
LIG(-125,115,-220,115)
LIG(-115,105,-135,105)
LIG(-415,115,-325,115)
LIG(-120,135,-140,135)
LIG(-395,125,-215,125)
LIG(-265,30,-225,30)
LIG(-425,20,-425,-5)
LIG(-370,20,-425,20)
LIG(-265,10,-265,30)
LIG(-415,40,-415,-5)
LIG(-420,30,-420,-5)
LIG(-370,40,-415,40)
LIG(-370,30,-420,30)
LIG(-410,50,-410,-5)
LIG(-370,50,-410,50)
LIG(-400,60,-400,-5)
LIG(-370,60,-400,60)
LIG(-395,70,-395,-10)
LIG(-370,70,-395,70)
LIG(-300,10,-320,10)
LIG(-320,10,-320,20)
LIG(-335,40,-270,40)
LIG(-335,135,-230,135)
LIG(-335,20,-320,20)
LIG(-325,115,-220,115)
LIG(-335,30,-325,30)
LIG(-335,50,-335,135)
LIG(-325,30,-325,115)
LIG(-270,40,-270,30)
LIG(-270,30,-270,30)
LIG(-385,-5,-385,90)
LIG(-385,90,-370,90)
LIG(-370,80,-390,80)
LIG(-390,-5,-390,80)
LIG(-135,105,-300,105)
LIG(-300,105,-300,10)
FFIG C:\Users\AcidRobot\github\clock_work\twelve_sixty.sch
